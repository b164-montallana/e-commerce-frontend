import { Form, Button, Container } from "react-bootstrap";
import { useNavigate, Navigate } from "react-router-dom";
import { useState, useContext, useEffect} from "react";
import UserContext from "../UserContext";
import Swal from "sweetalert2";


export default function Register() {
    const { user, setUser } = useContext(UserContext);

    const navigate = useNavigate()

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");

    const [isActive, setIsActive] = useState(false);

    function registerUser(e) {

		//prevents page redirection via a form submission
		e.preventDefault();

		fetch("https://capstone3ecommerce.herokuapp.com/api/users/checkEmail", {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Kindly provide another email to complete the registration"
				})
			} else {

				fetch("https://capstone3ecommerce.herokuapp.com/api/users/register", {
					method: "POST",
					headers: {
						"Content-Type": 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {

					if(data === true) {
						//Clear input fields
						setFirstName("");
						setLastName("");
						setEmail("");
						setMobileNo("");
						setPassword1("");
						setPassword2("");

						Swal.fire({
							title: 'Registration Successful',
							icon: 'success',
							text: 'Welcome to Batch 164, Course Booking!'
						})

						navigate("/login")

					} else {

						Swal.fire({
							title: 'Something went wrong!',
							icon: 'error',
							text: 'Please try again.'
						})
					}
				})

			}
		})

	}
    useEffect(()=> {
		//Validation to enable the submit button when all the input fields are populated and both passwords match 
		if((firstName !== "" && lastName !== "" && mobileNo.length === 11 && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNo, email, password1, password2])


  return (
    (user.id !== null) ?
    <Navigate to ="/products" />
    :
    <Container className="px-5">
    <Form onSubmit= {(e) => registerUser(e)} className="p-5">
        <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>First Name</Form.Label>
            <Form.Control 
                type="text" 
                placeholder="Enter First Name" 
                value={firstName}
                onChange={e => setFirstName(e.target.value)}
                />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Last Name</Form.Label>
            <Form.Control 
                type="text" 
                placeholder="Enter Last Name" 
                value={lastName}
                onChange={e => setLastName(e.target.value)}
                />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control 
                type="email" 
                placeholder="Enter email" 
                value={email}
                onChange={e => setEmail(e.target.value)}
                />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Mobile Number</Form.Label>
            <Form.Control 
                type="number" 
                placeholder="Enter mobile number" 
                value={mobileNo}
                onChange={e => setMobileNo(e.target.value)}
                />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control 
                type="password" 
                placeholder="Password" 
                value={password1}
                onChange={e => setPassword1(e.target.value)}
                />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control 
                type="password" 
                placeholder="Confirm Password" 
                value={password2}
                onChange={e => setPassword2(e.target.value)}
                />
        </Form.Group>
        {
            isActive ?
            <Button variant="secondary" type="submit">Submit</Button>
            :
            <Button variant="secondary" type="submit" disabled>Submit</Button>
        }
    </Form>
    </Container>
  )
}
