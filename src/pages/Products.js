import React, { useState, useEffect} from 'react'
import { Container } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';


export default function Products() {
    const [ products, setProducts ] = useState([]);

    useEffect(() => {
        fetch("https://capstone3ecommerce.herokuapp.com/api/products/active" )
        .then(res => res.json())
        .then(data => {
    
            setProducts(data.map(products => {
                return(
                    <ProductCard key={products._id} productProp={products}/>
                )
            }))
        })
    })
  return (
    
    <Container>
      {products}
    </Container>
    
  
  )
}
