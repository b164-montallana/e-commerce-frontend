
import { Container } from 'react-bootstrap';
import AdminView from '../components/AdminView';



export default function Admin() {
 
  return (
    <Container>
      <AdminView/>
    </Container>
  )
}
