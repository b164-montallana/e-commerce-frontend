import { Form, Button, Container } from "react-bootstrap"
import { useContext, useState, useEffect, Fragment } from "react";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import { Navigate } from "react-router-dom";

export default function SignIn() {

    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);
    // const [passwordShown, setPasswordShown] = useState(false);

    function userLogin(e) {

        e.preventDefault();

        /*
        Syntax:
            fetch("URL", {options})
            .then(res => res.json)
            .then(data => {})
        */

        fetch('https://capstone3ecommerce.herokuapp.com/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);
            if(typeof data.accessToken !== "undefined") {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            
                Swal.fire({
                    title: "Login is successful",
                    icon: 'success',
                    text: 'Welcome'
                })
            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: 'error',
                    text: 'Check your login credentials'
                })
            }
        })

        setEmail("");
        setPassword("");

        /*
        Syntax:
            localStorage.setItem("propertyName", value)
        */
        // localStorage.setItem("email", email)

        //Set the global user state to have properties from local storage
        // setUser({
        //     email: localStorage.getItem('email')
        // })

        const retrieveUserDetails = (token) => {
            fetch('https://capstone3ecommerce.herokuapp.com/api/users/details', {
                headers: {
                    Authorization: `Bearer ${ token }`
                }
            })
            .then(res => res.json())
            .then(data => {
                // console.log(data)

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
        }

    }    



    useEffect (() => {
        if(email !== "" && password !== ""){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

  return (
    (user.isAdmin === true) ?
    <Navigate to='/admin'/>
    :
    (user.id !== null) ?
    <Navigate to="/products"/>
    :
    <Fragment>
    <Container className="p-5">
        <h1 className="text-center">Sign In</h1>
        <Form onSubmit={(e) => userLogin(e)} className="px-5">
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter Email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    />
                    
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Enter Password" 
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    />
            </Form.Group>
            
            {
                isActive ?
                <Button variant="secondary" type="submit">Submit</Button>
                :
                <Button variant="secondary" type="submit" disabled>Submit</Button>
            }
        </Form>
    </Container>
    </Fragment>
  )
}
