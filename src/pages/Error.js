import { Fragment } from "react";
import { Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function ErrorPage() {
    
    return(
        <Fragment>
            <Row className="mt-5 px-5">
                <Col>
                    <h1 className="text-danger">404 Page not found</h1>
                <Button as={Link} to="/products">Home</Button>
                </Col>
            </Row>
        </Fragment>
    )
};

