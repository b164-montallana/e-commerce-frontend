import { useState, useEffect, useContext } from "react";
import OrderCard from "../components/OrderCard";
import { Container, Button } from "react-bootstrap";
import UserContext from "../UserContext";
import { Link } from "react-router-dom";

export default function Order() {
  const [ orders, setOrders ] = useState([]);
  const token = localStorage.getItem("token");
  const {user, setUser} = useContext(UserContext);

  useEffect(() => {
      fetch("https://capstone3ecommerce.herokuapp.com/api/order/my-orders", {
        headers:"GET",
        headers: {
          Authorization: `Bearer ${ token }`
      }
      })
      .then(res => res.json())
      .then(data => {
          setOrders(data.map(orders => {
              return(
                  <OrderCard key={orders._id} orderProp={orders}/>
              )
          }))
      })
  })
return (
  
    (user.id !== null) ?
    <Container>
    <h1 className="text-center my-3">Order History</h1>
    {orders}
    </Container>
    : 
    <Container className="my-5 text-center">
    <h1>Please Log in to view order</h1>
    <Button variant="info" as={Link} to={"/signin"}>Sign in</Button>
    
  </Container>
)
}
