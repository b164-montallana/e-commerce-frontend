import { Fragment, useContext, useEffect, useState } from "react";
import UserContext from "../UserContext";
import { Container, Row, Col, Card, Button, Nav, Form } from "react-bootstrap";
import { Link, useParams, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";


export default function ProductView() {

    const { user  } = useContext(UserContext);

    // const navigate = useNavigate();

    const { productId } = useParams();

    const navigate = useNavigate();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [stock, setStock] = useState(0);
    const [quant, setQuant] = useState(0);

    const addQuant = () => {
        setQuant(quant + 1)
      }
       
      const minusQuant = () => {
        if(quant >= 1) {
          setQuant(quant - 1)
        }
      }

    
    const addOrder = (productId) => {
        fetch('https://capstone3ecommerce.herokuapp.com/api/order/', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                userId: user.id,
                name: name,
                quantity: quant,
                amount: price
            })
            
        })
        .then(res => res.json())
		.then(data => {
			//console.log(data)

			if(data !== true) {
				Swal.fire({
					title:"Added to order.",
					icon:"success",
					
				})

				navigate('/products')

			} else {
				Swal.fire({
					icon:"error",
		
				})
			}
		})
	};

    
    useEffect(() => {
        console.log(productId)
        fetch(`https://capstone3ecommerce.herokuapp.com/api/products/${productId}`)
        .then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setStock(data.stock)
		});


	}, [productId])


  return (
    <Fragment>
        <Nav.Link className="m-5" as={Link} to='/products'>Back</Nav.Link>
        <Container className="mt-5">
            <Row>
                <Col lg={{span:6, offset:3}}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>PHP {price}</Card.Subtitle>
                            <Card.Subtitle>Stocks:</Card.Subtitle>
                            <Card.Text>{stock}</Card.Text>


                            <Form>
							  <Form.Group className="mb-3" controlId="formBasicEmail">
							    <Card.Text>Quantity: {quant}</Card.Text>
							    <Button variant="danger" onClick={() => minusQuant()}>-</Button>
							    <Button variant="success" onClick={() => addQuant()}>+</Button>
							  </Form.Group>
						
							</Form>

                            {
                                user.id !== null ?
								<Button variant="success" onClick={() => addOrder(productId)}>Add Order</Button>
                                :
                                <Link className="btn btn-danger" to="/signin">Log in</Link>

                            }

                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    </Fragment>
  )
}
