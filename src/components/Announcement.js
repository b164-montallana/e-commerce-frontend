import styled from "styled-components";

const Container = styled.div`
  height: 30px;
  background-color: #EF8354;
  color: white;
  display: flex;
  justify-content: center;
  font-size: 14px;
  font-weight: 500;
  padding-top: 5px;
`;

const Announcement = () => {
  return <Container>Super Deal! Free Shipping on Orders Over P500</Container>;
};

export default Announcement;
