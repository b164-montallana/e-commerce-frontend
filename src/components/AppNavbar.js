import { Fragment, useContext } from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { BsCart3 } from "react-icons/bs";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";


export default function AppNavbar() {

    const {user, setUser} = useContext(UserContext);

    return (
    <Navbar bg="dark" variant="dark" expand="md">
    <Container>
        <Navbar.Brand as={Link} to="/">The Camel Trader</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
            <Nav
                className="col d-flex justify-content-end me-auto my-2 my-lg-0"
                style={{ maxHeight: '100%' }}
                navbarScroll
                >

                {(user.isAdmin === true) ?
                <Fragment>
                    <Nav.Link as={Link} to="/admin">ADMIN</Nav.Link>
                    <Nav.Link as={Link} to="/products" >PRODUCTS</Nav.Link>
                    <Nav.Link as={Link} to="/logout" className="text-danger">LOGOUT</Nav.Link>
                </Fragment>
                :
                (user.id !== null) ?
                <Fragment>
                    <Nav.Link as={Link} to="/">HOME</Nav.Link>
                    <Nav.Link as={Link} to="/products" >PRODUCTS</Nav.Link>
                    <Nav.Link as={Link} to="/logout" className="text-danger">LOGOUT</Nav.Link>
                    <Nav.Link as={Link} to="/orders" >ORDERS</Nav.Link>
                </Fragment>
                :
                <Fragment>
                    <Nav.Link as={Link} to="/">HOME</Nav.Link>
                    <Nav.Link as={Link} to="/products" >PRODUCTS</Nav.Link>
                    <Nav.Link as={Link} to="/register" >REGISTER</Nav.Link>
                    <Nav.Link as={Link} to="/signin" >SIGN IN</Nav.Link>
                    <Nav.Link as={Link} to="/orders" >
                        ORDERS
                    </Nav.Link>
                </Fragment>
                }
            </Nav>
        </Navbar.Collapse>
    </Container>
  </Navbar>
    )
}




