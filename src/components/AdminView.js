import { Button, Modal, Form, Table, Nav, Container } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import React from 'react';
import Swal from 'sweetalert2';

export default function AdminView() {

  const [product, setProduct] = useState ([]);
  const [products, setProducts] = useState([]);
  const [productId, setProductId] = useState("");
  //console.log(props)
  const [name, setName] = useState("");
  const [desc, setDesc] = useState("");
  const [cat, setCat] = useState("");
  const [price, setPrice] = useState(0);
  const [stock, setStock] = useState(0);
  const [isActive, setIsActive] = useState(false);
  const [showAdd, setShowAdd] = useState(false);
  const [showEdit, setShowEdit] = useState(false);
  const [showDel, setShowDel] = useState(false);

  //function to handle opening and closing modals
  const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)
  //function open and close delete button
  const openDel = () => setShowDel(true);
  const closeDel = () => setShowDel(false);

  const token = localStorage.getItem("token")

  const openEdit = (productId) => {
		//fetch request with the course's ID
		fetch(`https://capstone3ecommerce.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			 console.log(data)
       setProductId(data._id)
       setName(data.name)
       setDesc(data.description)
       setPrice(data.price)
       setStock(data.stock)
       setCat(data.categories)
 
		})
		//populate the edit modal's input fields with the proper information
    setShowEdit(true)
	}

  
  const closeEdit = () => {
		setName("")
		setDesc("")
		setCat("")
		setPrice(0)
		setStock(0)
		setShowEdit(false)
  }

  //ADD A PRODUCT
  function addProduct(e) {
    e.preventDefault();

    fetch('http://localhost:4000/api/products/', {
      method: "POST",
      headers: {
        "Content-Type": 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify({
        name: name,
        description: desc,
        price: price,
        categories: cat,
        stock: stock,
        isActive: isActive
      })
    })
    .then(res => res.json())
    .then(data => {
      
      if(data) {
        Swal.fire({
          icon: 'success',
          text: 'Success'
        })

        setName("");
        setDesc("");
        setPrice("");
        setCat("");
        setStock("");
        setIsActive("");

        // fetchData()
        closeAdd()

      } else {
        
        Swal.fire({
          icon: 'error',
          text: 'Please try again'
        })
      }
    })
  }

  //EDIT PRODUCTS
  const editProducts = (e) => {
		e.preventDefault()

		fetch(`http://localhost:4000/api/products/update/${productId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
        description: desc,
        price: price,
        categories: cat,
        stock: stock,
        isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully updated"
				})

				// fetchData()
				closeEdit()

				setName("");
        setDesc("");
        setPrice("");
        setCat("");
        setStock("");
        setIsActive("");
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})

				
			}
		})
	}

  //ARCHIVE A PRODUCT
  const archiveToggle = (productId, isActive) => {
		fetch(`https://capstone3ecommerce.herokuapp.com/api/products/archive/${productId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data){
				let bool

				isActive ? bool = "disabled" : bool = "enabled"

				// fetchData()

				Swal.fire({
					title: "Sucess",
					icon: "success",
					text: `Course successfully ${bool}`
				})
			}else{
				// fetchData()

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}

		})
	}

  

  //GET PRODUCTS
  useEffect(() => {
    fetch("https://capstone3ecommerce.herokuapp.com/api/products/all")
    .then(res => res.json())
    .then(data => {

        setProducts(data.map(product => {
          return(
              <tr key={product._id}>
                <td>{product.name}</td>
                <td>{product.description}</td>
                <td>{product.categories}</td>
                <td>{product.price}</td>
                <td>{product.stock}</td>
                <td>
                    {product.isActive
                      ? <span>Available</span>
                      : <span>Unavailable</span>
                    }
                </td>
                <td>
                <Button className="my-1" variant="primary" size="sm" onClick={() => openEdit(product._id)} >Update</Button>
                { (product.isActive )
                ? <Button variant="danger" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Disable</Button>
                : <Button variant="danger" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Enable</Button>
                }
				      	</td>

              </tr>
          )
      }))
        
    })
}, [products])

  return (
    <Container>
      <div className='text-center mt-4'>
        <h2>Admin Dashboard</h2>
        <Button onClick={openAdd}>Add New Product</Button>
      </div>
      
      {/*Products info table*/}
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Category</th>
						<th>Price</th>
						<th>Stocks</th>
						<th>Availability</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
          {products}
				</tbody>
			</Table>


     {/* Add Product Modal */}
     <Modal show={showAdd} onHide={closeAdd}>
        <Form onSubmit={e => addProduct(e)}>
          <Modal.Header closeButton>
            <Modal.Title>Add Product</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group controlId="productName">
              <Form.Label>Name</Form.Label>
              <Form.Control
                value={name}
								onChange={e => setName(e.target.value)}
								type="text"
								required
              />
            </Form.Group>

            <Form.Group controlId="productDesc">
              <Form.Label>Description</Form.Label>
              <Form.Control
                value={desc}
								onChange={e => setDesc(e.target.value)}
								type="text"
								required
              />
            </Form.Group>

            <Form.Group controlId="productCat">
              <Form.Label>Category</Form.Label>
              <Form.Control
                value={cat}
								onChange={e => setCat(e.target.value)}
								type="text"
								required
              />
            </Form.Group>

            <Form.Group controlId="productPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                value={price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
              />
            </Form.Group>

            <Form.Group controlId="productStock">
              <Form.Label>Stocks</Form.Label>
              <Form.Control
                value={stock}
								onChange={e => setStock(e.target.value)}
								type="number"
								required
              />
            </Form.Group>

            <Form.Group controlId="productIsActive">
              <Form.Label>isActive</Form.Label>
              <Form.Control
                value={isActive}
								onChange={e => setIsActive(e.target.value)}
								type="text"
								required
              />
            </Form.Group>

          </Modal.Body>
          <Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>

        </Form>
     </Modal>

      {/* Update Prodcuts Modal */}
      <Modal show={showEdit} onHide={closeEdit}>
        <Form onSubmit={e => editProducts(e)}>
          <Modal.Header closeButton>
            <Modal.Title>Update</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group controlId="productName">
              <Form.Label>Name</Form.Label>
              <Form.Control
                value={name}
								onChange={e => setName(e.target.value)}
								type="text"
								required
              />
            </Form.Group>

            <Form.Group controlId="productDesc">
              <Form.Label>Description</Form.Label>
              <Form.Control
                value={desc}
								onChange={e => setDesc(e.target.value)}
								type="text"
								required
              />
            </Form.Group>

            <Form.Group controlId="productCat">
              <Form.Label>Category</Form.Label>
              <Form.Control
                value={cat}
								onChange={e => setCat(e.target.value)}
								type="text"
								required
              />
            </Form.Group>

            <Form.Group controlId="productPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                value={price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
              />
            </Form.Group>

            <Form.Group controlId="productStock">
              <Form.Label>Stocks</Form.Label>
              <Form.Control
                value={stock}
								onChange={e => setStock(e.target.value)}
								type="number"
								required
              />
            </Form.Group>

            <Form.Group controlId="productIsActive">
              <Form.Label>isActive</Form.Label>
              <Form.Control
                value={isActive}
								onChange={e => setIsActive(e.target.value)}
								type="text"
								required
              />
            </Form.Group>

          </Modal.Body>
          <Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>

        </Form>
     </Modal>

      {/* Delete Product modal
      <Modal show={showDel} onHide={closeDel}>
        <Modal.Header closeButton>
          <Modal.Title>Are you sure you want to delete product?</Modal.Title>
        </Modal.Header>
        <Nav className='d-flex justify-content-center'>
          <Nav.Link className='text-danger' onClick={() => deleteProduct()}>Proceed</Nav.Link>
        </Nav>
      </Modal> */}
    </Container>
  )
}

