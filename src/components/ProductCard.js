import { Card, Container, Button, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom';


export default function ProductCard({productProp}) {

    const {name, description, price, category, _id} = productProp

  return (
    <Container className='mt-3'>
        <Col>
          <Card className='p-0 overflow-hidden h-100 shadow'>
              <Card.Body>
                  <Card.Title>{name}</Card.Title>
                  <Card.Subtitle>Description:</Card.Subtitle>
                  <Card.Text>{description}</Card.Text>
                  <Card.Text>{price}</Card.Text>
                  <Button variant='primary' as={Link} to={`/products/${_id}`}>Details</Button>
              </Card.Body>
          </Card>
        </Col> 
    </Container>
  )
}
